import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import PhpView from '../views/languagesViews/PhpView.vue'
import HtmlView from '../views/languagesViews/HtmlView.vue'
import WordpressView from '../views/cmsViews/WordpressView.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'home',
        component: HomeView
    },
    {
        path: '/a-propos',
        name: 'about',
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/AboutView.vue')
    },
    {
        path: '/html',
        name: 'html',
        component: HtmlView
    },
    {
        path: '/php',
        name: 'php',
        component: PhpView
    },
    {
        path: '/wordpress',
        name: 'wordpress',
        component: WordpressView
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router